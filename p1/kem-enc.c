/* kem-enc.c
 * simple encryption utility providing CCA2 security.
 * based on the KEM/DEM hybrid model. */

#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <string.h>
#include <fcntl.h>
#include <openssl/sha.h>
//#include <sys/mman.h>

#include "ske.h"
#include "rsa.h"
#include "prf.h"

static const char* usage =
"Usage: %s [OPTIONS]...\n"
"Encrypt or decrypt data.\n\n"
"   -i,--in     FILE   read input from FILE.\n"
"   -o,--out    FILE   write output to FILE.\n"
"   -k,--key    FILE   the key.\n"
"   -r,--rand   FILE   use FILE to seed RNG (defaults to /dev/urandom).\n"
"   -e,--enc           encrypt (this is the default action).\n"
"   -d,--dec           decrypt.\n"
"   -g,--gen    FILE   generate new key and write to FILE{,.pub}\n"
"   -b,--BITS   NBITS  length of new key (NOTE: this corresponds to the\n"
"                      RSA key; the symmetric key will always be 256 bits).\n"
"                      Defaults to %lu.\n"
"   --help             show this message and exit.\n";

#define FNLEN 255

enum modes {
	ENC,
	DEC,
	GEN
};

/* Let SK denote the symmetric key.  Then to format ciphertext, we
 * simply concatenate:
 * +------------+----------------+
 * | RSA-KEM(X) | SKE ciphertext |
 * +------------+----------------+
 * NOTE: reading such a file is only useful if you have the key,
 * and from the key you can infer the length of the RSA ciphertext.
 * We'll construct our KEM as KEM(X) := RSA(X)|H(X), and define the
 * key to be SK = KDF(X).  Naturally H and KDF need to be "orthogonal",
 * so we will use different hash functions:  H := SHA256, while
 * KDF := HMAC-SHA512, where the key to the hmac is defined in ske.c
 * (see KDF_KEY).
 * */

#define HASHLEN 32 /* for sha256 */

int kem_encrypt(const char* fnOut, const char* fnIn, RSA_KEY* K)
{
//	FILE * inFile;
	FILE * outFile;
//	inFile = fopen(fnIn,"r");
	outFile = fopen(fnOut,"w+");

	//encrypt inBuf with symmetric key
	size_t rsaLen = rsa_numBytesN(K);
	unsigned char* x = calloc(rsaLen,1);

	x[rsaLen-1] = 0;
	randBytes(x,rsaLen-1);

	//encapsulate SK using rsa and SHA256
	unsigned char* encaps = calloc(rsaLen,1);

	rsa_encrypt(encaps,x,rsaLen, K);
	unsigned char* outBuf = calloc(rsaLen+HASHLEN,1);
	unsigned char* hash = calloc(HASHLEN,1);
	SHA256_CTX sha256;
    	SHA256_Init(&sha256);
    	SHA256_Update(&sha256, x, HASHLEN);
    	SHA256_Final(hash, &sha256);

	memcpy(outBuf,encaps, rsaLen);
	memcpy(outBuf+rsaLen,hash,HASHLEN);
	fwrite(outBuf,rsaLen+HASHLEN,1,outFile);
	fclose(outFile);

//printf("old x:   %s\n", x);
//printf("hash from old x: %s\n", hash);
	SKE_KEY SK;
	ske_keyGen(&SK,x,rsaLen);
//printf("old x hmackey %s\n", SK.hmacKey);
//printf("old x aeskey  %s\n", SK.aesKey);


	size_t offset = rsaLen+HASHLEN;
	ske_encrypt_file(fnOut, fnIn, &SK, NULL, offset);


	/* TODO: encapsulate random symmetric key (SK) using RSA and SHA256;
	 * encrypt fnIn with SK; concatenate encapsulation and cihpertext;
	 * write to fnOut. */

	return 0;
}

/* NOTE: make sure you check the decapsulation is valid before continuing */
int kem_decrypt(const char* fnOut, const char* fnIn, RSA_KEY* K)
{
	FILE * inFile;
	inFile = fopen(fnIn,"r");

	fseek(inFile,0,SEEK_END);
	size_t inSize = ftell(inFile);
	rewind(inFile);
	unsigned char* inBuf = calloc(inSize,1);
	fread(inBuf,1,inSize,inFile);

	//get encaps and extract the sk
	size_t rsaLen = rsa_numBytesN(K);
	unsigned char* encaps = calloc(rsaLen,1);
	memcpy(encaps,inBuf,rsaLen);

	unsigned char* sk = calloc(rsaLen,1);
	//sk[rsaLen-1] = 0;
	//randBytes(sk,rsaLen-1);

	memcpy(sk,encaps,rsaLen);
	rsa_decrypt(sk,encaps,rsaLen, K);

	//check hash of sk with the rest of encaps
	unsigned char* hash = calloc(HASHLEN,1);
	//printf("empty hash: %s\n", hash);
	SHA256_CTX sha256;
    	SHA256_Init(&sha256);
    	SHA256_Update(&sha256, sk, HASHLEN);
    	SHA256_Final(hash, &sha256);
	unsigned char* sk_hash = calloc(HASHLEN,1);
	//printf("empty skhash %s\n", sk_hash);
	memcpy(sk_hash, inBuf+rsaLen, HASHLEN);
//	printf("hash %s\n", hash);
//	printf("skhash %s\n", sk_hash);
//	printf("new x:   %s\n", sk);
//	printf("hash from new x: %s\n", hash);
	size_t offset = rsaLen+HASHLEN;
	SKE_KEY SK;
	ske_keyGen(&SK,sk,rsaLen);

//	printf("new x hmackey %s\n", SK.hmacKey);
//	printf("new x aeskey  %s\n", SK.aesKey);
	if(strcmp((char*)hash,(char*)sk_hash)){
		//printf("oh no %s\n", "");
		return -1;
	}

	ske_decrypt_file(fnOut,fnIn,&SK,offset);

	/* TODO: write this. */
	/* step 1: recover the symmetric key */
	/* step 2: check decapsulation */
	/* step 3: derive key from ephemKey and decrypt data. */
	return 0;
}

int main(int argc, char *argv[]) {
	/* define long options */
	static struct option long_opts[] = {
		{"in",      required_argument, 0, 'i'},
		{"out",     required_argument, 0, 'o'},
		{"key",     required_argument, 0, 'k'},
		{"rand",    required_argument, 0, 'r'},
		{"gen",     required_argument, 0, 'g'},
		{"bits",    required_argument, 0, 'b'},
		{"enc",     no_argument,       0, 'e'},
		{"dec",     no_argument,       0, 'd'},
		{"help",    no_argument,       0, 'h'},
		{0,0,0,0}
	};
	/* process options: */
	char c;
	int opt_index = 0;
	FILE* file;
	FILE* keyfile;
	RSA_KEY K;
	char fnRnd[FNLEN+1] = "/dev/urandom";
	fnRnd[FNLEN] = 0;
	char fnIn[FNLEN+1];
	char fnOut[FNLEN+1];
	char fnKey[FNLEN+1];
	memset(fnIn,0,FNLEN+1);
	memset(fnOut,0,FNLEN+1);
	memset(fnKey,0,FNLEN+1);
	int mode = ENC;
	// size_t nBits = 2048;
	size_t nBits = 1024;
	while ((c = getopt_long(argc, argv, "edhi:o:k:r:g:b:", long_opts, &opt_index)) != -1) {
		switch (c) {
			case 'h':
				printf(usage,argv[0],nBits);
				return 0;
			case 'i':
				strncpy(fnIn,optarg,FNLEN);
				break;
			case 'o':
				strncpy(fnOut,optarg,FNLEN);
				break;
			case 'k':
				strncpy(fnKey,optarg,FNLEN);
				break;
			case 'r':
				strncpy(fnRnd,optarg,FNLEN);
				break;
			case 'e':
				mode = ENC;
				break;
			case 'd':
				mode = DEC;
				break;
			case 'g':
				mode = GEN;
				strncpy(fnOut,optarg,FNLEN);

				break;
			case 'b':
				nBits = atol(optarg);
				break;
			case '?':
				printf(usage,argv[0],nBits);
				return 1;
		}
	}

	/* TODO: finish this off.  Be sure to erase sensitive data
	 * like private keys when you're done with them (see the
	 * rsa_shredKey function). */
	switch (mode) {
		case ENC:
			keyfile = fopen(fnKey,"r");
			rsa_readPublic(keyfile,&K);
			kem_encrypt(fnOut,fnIn,&K);
                        fclose(keyfile);
                        rsa_shredKey(&K);
			break;
		case DEC:
			keyfile = fopen(fnKey,"r");
			rsa_readPrivate(keyfile,&K);
			kem_decrypt(fnOut, fnIn, &K);
			fclose(keyfile);
			rsa_shredKey(&K);
                        break;
		case GEN:
			file = fopen(fnOut,"w");
			rsa_keyGen(nBits,&K);
			rsa_writePrivate(file,&K);
			fclose(file);
			file = fopen(strcat(fnOut,".pub"),"w");
			rsa_writePublic(file,&K);
			fclose(file);
                        rsa_shredKey(&K);
                        break;
		default:
			return 1;
	}
	return 0;
}
